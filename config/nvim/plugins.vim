if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
  echo "Downloading junegunn/vim-plug to manage plugins..."
  silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
  silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
  autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

Plug 'preservim/nerdtree'
Plug 'preservim/tagbar'

Plug 'easymotion/vim-easymotion'            "<Leader><Leader>+ w,f(target),s

Plug 'tpope/vim-surround'                   " add command to create surrounding parens: cs"' (change), ds" (delete), yss) (line)
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people' "slurp/barf: >), <), >(, <(, <f, >f, <e, >e, <I, >I, dsf(splice), cse(, cse[, cse{
Plug 'w0rp/ale'                             "ALE: asynchronous lint engine

Plug 'tpope/vim-fugitive'                   " add command to show git info in status line
Plug 'itchyny/lightline.vim'                " status line
"Plug 'bling/vim-airline'

Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'     "fuzzy finder to rule them all
Plug 'airblade/vim-rooter'

"Plug 'SirVer/ultisnips'  "snippets but needs python3
"Plug 'honza/vim-snippets'

Plug 'Olical/conjure', {'tag': 'v4.3.1'}  " clojure
Plug 'tpope/vim-dispatch'                 " run commands in the background (required by vim-jack-in)
Plug 'radenling/vim-dispatch-neovim'      " adds support to neovim's terminal emulator and job control
Plug 'clojure-vim/vim-jack-in'            " Jack-In to repl
Plug 'kelvin-mai/vim-cljfmt'              " clojure formatter
Plug 'luochen1990/rainbow'                " rainbow parens
Plug 'sheerun/vim-polyglot'               " better default syntax
"Plug 'ryanoasis/vim-devicons'

"Plug 'neoclide/coc.nvim', {'branch': 'release' } "Intellisense
"Plug 'tpope/vim-commentary'                 " comments

"Plug 'junegunn/goyo.vim'
"Plug 'kjssad/quantum.vim'                 " firefox theme
"Plug 'pangloss/vim-javascript'            " javascript
"Plug 'leafgarland/typescript-vim'         " typescript 
"Plug 'maxmellon/vim-jsx-pretty'           " js / jsx 
"Plug 'peitalin/vim-jsx-typescript'        " tsx
"Plug 'jparise/vim-graphql'                " graphql
"Plug 'reasonml-editor/vim-reason-plus'    " reasonml
"Editor features
"Plug 'jiangmiao/auto-pairs', { 'tag': 'v2.0.0' }   " automatic paren completion
"Plug 'airblade/vim-gitgutter'             " git gutter
"Plug 'rhysd/conflict-marker.vim'          " git conflict marker

call plug#end()

" Automatically install missing plugins on startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif


