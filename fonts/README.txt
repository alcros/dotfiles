
1. create new directory
mkdir -p $HOME/.local/share/fonts/NEWFONT

2. copy font files to new directory
cp ~/Downloads/NEWFONT.* ~/.local/share/fonts/NEWFONT

3. update font cache
fc-cache -v

