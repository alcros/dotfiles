# User configuration
export TERMINAL=urxvt
export EDITOR=vim
export BROWSER=brave
export TZ=America/Santiago

export LC_ALL="en_US.UTF-8"
export LC_COLLATE="C"
export LANG="en_US.UTF-8"

# setup PATH variable
path+=($(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//'))
path+=($HOME/.scripts)
path+=($HOME/bin)

# source this OS' custom environment variables
source $HOME/dotfiles/zshenv.custom

export PATH

