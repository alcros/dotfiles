#!/bin/sh

error() {
  clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

create_links_msg() { \
  #dialog --title "Instalacion" --infobox "Creando links..." 5 70
  echo "Instalación"
  echo "Creando links..."
}

show_warning_msg() {
  #dialog --title "Importante !!!" --yesno "Por ahora, corre este script desde el dir $HOME/dotfiles, de otra forma los links no se crearan bien.\\n\\nTu directorio actual es: $PWD\\n\\nDeseas continuar?" 10 70
  echo "Importante !!!"
  echo "Por ahora, corre este script desde el dir $HOME/dotfiles, de otra forma los links no se crearan bien."
  echo "Tu directorio actual es: $PWD"
  echo "Deseas continuar?"
}

create_links() {
  [ -d "$HOME/.config" ] || mkdir $HOME/.config
  ln -svf $(readlink -f Xmodmap) $HOME/.Xmodmap
  ln -svf $(readlink -f Xresources) $HOME/.Xresources
  ln -svf $(readlink -f bashrc) $HOME/.bashrc
  ln -svf $(readlink -f calcurse) $HOME/.calcurse
  ln -svf $(readlink -f crontab) $HOME/.crontab
  ln -svf $(readlink -f gitconfig) $HOME/.gitconfig
  ln -svf $(readlink -f gitignore) $HOME/.gitignore
  ln -svf $(readlink -f gvimrc) $HOME/.gvimrc
  ln -svf $(readlink -f mailcap) $HOME/.mailcap
  ln -svf $(readlink -f mbsyncrc) $HOME/.mbsyncrc
  ln -svf $(readlink -f mpdconf) $HOME/.mpdconf
  ln -svf $(readlink -f msmtprc) $HOME/.msmtprc
  ln -svf $(readlink -f muttrc) $HOME/.muttrc
  ln -svf $(readlink -f spacemacs) $HOME/.spacemacs

  [ -d "$HOME/.ssh" ] || mkdir $HOME/.ssh
  [ -f "$HOME/.ssh/config" ] || cp ssh/config $HOME/.ssh/
  cp ssh/*.pub $HOME/.ssh/

  ln -svf $(readlink -f tmux.conf) $HOME/.tmux.conf
  ln -svf $(readlink -f vim) $HOME/.vim
  ln -svf $(readlink -f vimrc) $HOME/.vimrc

  ln -svf $(readlink -f xinitrc) $HOME/.xinitrc
  ln -svf $(readlink -f zgen) $HOME/.zgen
  ln -svf $(readlink -f zlogin) $HOME/.zlogin
  ln -svf $(readlink -f zprofile) $HOME/.zprofile
  ln -svf $(readlink -f zshenv) $HOME/.zshenv
  ln -svf $(readlink -f zshrc-zgen-basic) $HOME/.zshrc

  #user utils
  ln -svnf $(readlink -f fonts) $HOME/.fonts
  ln -svf $(readlink -f scripts) $HOME/.scripts
  ln -svf $(readlink -f utils) $HOME/.utils
  ln -svf $(readlink -f wallpapers) $HOME/.wallpapers

  #config
  [ -d "$HOME/.config/bspwm" ] || mkdir $HOME/.config/bspwm
  ln -svf $(readlink -f config/bspwm/bspwmrc) $HOME/.config/bspwm/bspwmrc
  ln -svf $(readlink -f config/bspwm/autostart) $HOME/.config/bspwm/autostart
  [ -d "$HOME/.config/sxhkd" ] || mkdir $HOME/.config/sxhkd
  ln -svf $(readlink -f config/sxhkd/sxhkdrc.common) $HOME/.config/sxhkd/sxhkdrc.common
  ln -svf $(readlink -f config/sxhkd/sxhkdrc.custom) $HOME/.config/sxhkd/sxhkdrc.custom
  ln -svf $(readlink -f config/polybar) $HOME/.config/polybar
  ln -svf $(readlink -f config/picom.conf) $HOME/.config/picom.conf
  ln -svf $(readlink -f config/conky) $HOME/.config/conky
  ln -svf $(readlink -f config/dunst) $HOME/.config/dunst
  ln -svf $(readlink -f config/htop) $HOME/.config/htop
  ln -svf $(readlink -f config/lf) $HOME/.config/lf
  ln -svf $(readlink -f config/mc) $HOME/.config/mc
  ln -svf $(readlink -f config/sc-im) $HOME/.config/sc-im
  #ln -svf $(readlink -f config/minidlna) $HOME/.config/minidlna
  ln -svf $(readlink -f config/mpv) $HOME/.config/mpv
  ln -svf $(readlink -f config/ncmpcpp) $HOME/.config/ncmpcpp
  ln -svf $(readlink -f config/nvim) $HOME/.config/nvim
  ln -svf $(readlink -f config/sxiv) $HOME/.config/sxiv
  ln -svf $(readlink -f config/vifm) $HOME/.config/vifm
  ln -svf $(readlink -f config/zathura) $HOME/.config/zathura
}

# Create soft links to config files in dotfiles dir
create_links_msg || error "User exited."

show_warning_msg
create_links

