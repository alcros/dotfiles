#!/bin/sh

while getopts "p:r:h" o; do case "${o}" in
    p) pkgfile=${OPTARG} ;;
    r) role=${OPTARG} ;;
    h) printf "Argumentos opcionales:\\n  -p: Archivo CSV con paquetes a instalar\\n  -r: el rol a usar\\n  -h: Mostrar este mensaje\\n" && exit 1 ;;
    *) printf "Opcion invalida: -%s\\n" "$OPTARG" && exit 1 ;;
esac done

[ -z "$pkgfile" ] && pkgfile="packages.csv"
[ -z "$role" ] && role="U"

install_pkg(){
  sudo dnf install -y "$1" >/dev/null 2>&1 ;
}

error() {
  printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

welcome() {
  printf "Bienvenido!\n"
  printf "Este script te permitira inicializar un sistema Linux!\n"
  printf "Esta basado en los gustos y preferencias del autor.\n"
  printf " { Carlos }\n\n"
  printf "Nota Importante!\n"
  printf "  Asegúrate de que tu usuario pueda instalar paquetes en el sistema con \"sudo\" "
  printf "  sin necesidad de introducir tu password"
  printf "  Este script esta basado en las utilidades de Luke Smith (LARBS) para sistemas Arch Linux."
  printf "\n"
  printf "¿Continuamos? [si/No]\n"
  read answer
}

preinstall_msg() {
  echo "preinstall message"
}

role_msg() {
  printf "..:: Rol ::..\n"
  printf "Que tanto software debo instalar?\n\nEstas opciones son acumulativas, o sea, si\n"
  printf "escoges 'D', eso implica que se instalaran R, U y D.\n"
  printf "e.g.: R < U < D < T\n"
  printf "\n"
  printf "  - R   Lo minimo\n"
  printf "  - U   Homo Unix\n"
  printf "  - D   Desktop\n" 
  printf "  - o   Opcionales de terminal\n"
  printf "  - T   Todo en la lista\n\n"
  printf "¿rol?: "
  #read role
  printf "usando rol: $role\n\n"
}

main_install() {
  printf "Instalando '$1' ($n de $total) >> $2 \n"
  install_pkg "$1"
}

install_required() {
  for required in neofetch; do
    printf "..:: Instalacion ::..\n"
    printf "Instalando paquete requerido '$required'.\n"
    install_pkg "$required"
  done
}

install_from_list() { \
  printf "..:: Instalación ::..\n"
  [ -f "$pkgfile" ] && cp "$pkgfile" /tmp/progs.csv
  total=$(wc -l < /tmp/progs.csv)
  while IFS=, read -r tag package category desc; do
    n=$((n+1))
    case "$role"  in
        "R") [ $tag == "R" ] && main_install "$package" "Requeridos: $category:$desc" ;;
        "U") [ $tag == "U" -o $tag == "R" ] && main_install "$package" "Homo Unix: $category:$desc" ;;
        "X") [ $tag == "X" -o $tag == "U" -o $tag == "R" ] && main_install "X server: $category:$package" "$desc" ;;
        "D") [ $tag == "D" -o $tag == "X" -o $tag == "U" -o $tag == "R" ] && main_install "Desktop: $category:$package" "$desc" ;;
        "o") [ $tag == "o" -o $tag == "R" ] && main_install "Opcional de terminal: $category:$package" "$desc" ;;
        "O") [ $tag == "O" -o $tag == "X" ] && main_install "Opcional de X: $package" "$desc" ;;
        "T") main_install "$package" "$desc" || { clear; exit 1; } ;;
    esac
  done < /tmp/progs.csv ;}

finalize() {
  printf "..:: Hecho! ::..\n"
  printf "Felicidades! No parece haber fallado nada asi que el script finalizo exitosamente\n"
  printf "y todos los paquetes y archivos de configuracion deben estar en su lugar!\n"
  printf "  --Carlos\n"
}

tuning_it() {
  # make zsh the default shell for the user.
  printf "Cambiando a un shell superior"
  sudo chsh -s /bin/zsh `whoami` >/dev/null 2>&1
}

check_sudo(){
  sudo -n dnf install -y neofetch >/dev/null 2>&1
}

# MAIN
welcome || error "Este script necesita \'dialog\' para mostrar mensajes."

check_sudo || error "No puedes usar \'sudo\' sin password, ya revisé!"

role_msg || error "Algo sucedió seleccionando el rol."

# Last chance for user to back out before install.
#preinstall_msg || error "preinstall falló."

install_required || error "install failed."

# read the packages.csv file and install everything listed in there
install_from_list

tuning_it || error "tuning failed"

# Last message! Install complete!
finalize
clear
neofetch

